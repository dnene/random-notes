# Blockchain

Main reference used: [Minimum Viable Block Chain](https://www.igvita.com/2014/05/05/minimum-viable-block-chain/)

## Intent

* Provide a theoretical understanding of blockchain
* Why? Because the bottom up view of imperatives for specific characteristics and solutions as a result provide a much stronger understanding of blockchain than a top down view which often hides important details and is accompanied with a lot of spiel and/or hand waving
* Discuss some practical issues with Blockchain and how they are dealt with (by diluting some characteristics of blockchain)

## Bilateral System with Trust Provider

### Triple entry book keeping

* Alice and Bob need to trade
* Need for a notary - Chuck to sign
* Chuck is now the authenticator, trust provider. Chuck's books are authentic. Anything not in chuck's books is fake.
* * Authentication
* * Non repudiation
* * Integrity
* If Chuck colludes .. trust falls apart

### Using PKI

* PKI provides all capabilities - authentication, non repudiation, integrity
* Alice takes the message, encodes it, encrypts the encoded value using her private key, and envelopes it with a "signed by Alice" message
* This works .. Chuck not required

### Computing account balance

* Add a value to every transaction
* Balances can be computed by traversing the full ledger

## Multilateral system

### Transfer earlier transaction

* Bob can now use a encoded value of an earlier transaction, include it in a new field and transfer the amount to John
* John can get public keys of Bob and Alice, vet all earlier transactions, ensure the transferred transaction is authentic, _and that Bob has not transferred the same transaction to someone else_.
* John can now present the ledger to Alice and demand money from her
* Alice can also verify the ledger using Bob's public key

### New risk

* Bob can now choose to use an earlier version of the ledger and do one more transfer entry to yet another person. Or for that matter many more
* Problem will be found when multiple people present their ledger to Alice .. but it will be too late
* One could say all parties must always be present when any changes get made to the ledger and approve them, but thats too impractical
* Need for consensus
* * Unanimous
* * Quorum - What is Quorum?

### Limitations of prior approaches to Distributed consensus

* No idea about total number of participants. New ones could join. Offline ones may have left temporarily or permanently
* Sybil attacks - fake profiles
* Consensus tracking also hard. Consensus tracking could be solved by a centralised registry. But that makes the registry a single point of failure.
* Could use voting
* Only one vote can be cast by a party and can then be distributed to all others

### Identity issues
* Signers could generate unlimited number of private keys
* IDs could be implemented. IDs work only so long as cost of implementing fake ids is much larger than benefits. Else everyone will create fake ids.
* Alternative could be anything where cost of implementing it is much higher than benefits of implementing it
* Perhaps a really really expensive algorithm.

### Proof of work issues

* In a quorum based consensus, what is the quorum?
* If cost of proof of work is low, quorum will be high, if the cost of proof of work is high, quorum can be quite low. If cost is really really high, proof of work is more expensive than the transaction cost.
* Proof of work providers need to be compensated
* Also if multiple transactions can pooled together into one block .. cost of proof of work can be spread across multiple transaction participants across that block. Thus a block.
* If the blocks are chained together in a linked list (ie each block refers to the earlier block through an encoded identifier for the block), the last block in effect represents the full chain. A block chain.

### Role of proof of work provider - aka miners

* Transaction participants simply record transactions and toss them into a global pool
* Miners will wait for enough transactions to accumulate before the start creating a block (incentives on creating a block have to exceed cost to create one).
* Miner will now collect reference to the last block, a list of various transactions, and a transaction representing his own fee into a block and now attempts to solve the proof of work challenge
* Once miner generates a valid block, he will distribute it to all participants (and miners)
* Original transaction participants on receiving the block can verify that their transaction appears in the last node of that block chain.
* Miners on receiving the block will abandon their attempt to create another block, and instead restart the process with the new block as the last block and remaining transactions from the global pool

### But multiple valid blocks could get generated and pushed out

* Multiple miners could create and publish a new block at roughly the same time.
* This could result in different chains starting to be formed.
* Receivers when they receive alternate chains should choose the longest block and abandon the shorter blocks. This will lead to some transactions earlier considered authenticated to no longer be so.
* Still prone to attacks, but attackers now have a challenge to build a longer blockchain. Longer the chain, it is more expensive to build.

### Blocks are never final

* Only the longest blockchain wins
* Every participant needs to decide how deep in the blockchain does a transaction need to be before it is considered valid.

### Aside: What about balance? Eg. with Bitcoin

* This is only a transaction journal. Not really a ledger or a consolidated accounting balance maintained anywhere.
* Sort of worked around by recipient of value treating each transaction in a prior block as a currency note of corresponding value, which can be split into multiple payments to distribute to others including self.
* Once the new transaction is available on the blockchain, the older transaction is in effect no longer available to spend again

### Important characteristics of Blockchain

* Unlimited number of participants
* All data (ledger) is available to all participants
* There is no central trust provider. Multiple miners must compete. And longest block wins.
* Data is distributed, data is immutable (to the extend blocks are not replaced by a longer chain)
* There is no central processor, everyone runs their own service. Transaction costs (except for mining) are borne by each participant on their own behalf.

### Extensibility of blockchain

* Frankly could be extended to include any data of interest.
* A particularly useful use case is being extended by smart contracts .. ie. pieces of embedded logic inside every transaction. Eg. a lending transaction could contain a set of rules which automatically imply and require compensating future interest and principal repayment schedule. Thus money could be transferred back even without the borrower initiating a transaction.

### Proof of work is extremely expensive

* Bitcoin's energy consumption 'equals that of Switzerland' https://www.bbc.com/news/technology-48853230 .. also estimated at 0.21% of World's energy consumption. There's the carbon footprint also.
* Proof of stake - miners can still be added at will, but have some stake and have to submit some proof of stake, and the next miner will be selected at random based on the miner's stake. Although trust is distributed, on a per block basis, it is completely centralised.
* Proof of elapsed time
* Hyperledger
* Ripple

### Private blockchains - participants can be controlled (thus needs a central network controller)
* Issues
* * Access Control
* * Who has what powers
* * Efficiency
* Those with minimum characteristics from Blockchain are called distributed ledgers. 

### Important issues with Blockchain

* Democratisation and Disintermediation - Why would intermediaries invest in creating such platforms
* Openness - All data on default blockchain is public (or restricted to known parties at recording time). Would people prefer invoices, credit card spends, stock market orders etc to be public?
* Speed - How could one do HFT trading? A person can checkout from Starbucks only because transaction sizes are low, how long will it take to confirm a huge value transaction?
* Energy consumption

